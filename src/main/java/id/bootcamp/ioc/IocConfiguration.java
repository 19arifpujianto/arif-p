package id.bootcamp.ioc;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import id.bootcamp.repositories.M_Customer_RelationRepo;
import id.bootcamp.services.M_Customer_RelationServ;

@Configuration
public class IocConfiguration {

	@Bean
	M_Customer_RelationServ customerService(M_Customer_RelationRepo rr) {
		return new M_Customer_RelationServ(rr);
	}
}
