package id.bootcamp.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.bootcamp.models.M_Customer_Relation;
import id.bootcamp.repositories.M_Customer_RelationRepo;


@Transactional
public class M_Customer_RelationServ {
	
	@Autowired
	private M_Customer_RelationRepo cr;
	
	//Konstruktor
	public M_Customer_RelationServ(M_Customer_RelationRepo cr) {
		this.cr = cr;
	}

	
	public List<M_Customer_Relation> getAllCustomer(){
		return cr.findAll();
	}
	
	public M_Customer_Relation getCustomerById(Long id) {
		return cr.findById(id).get();
	}
	
	public void addCustomer(M_Customer_Relation c) {
		cr.save(c);
	}
	
	public void editCustomer(M_Customer_Relation c) {
		cr.save(c);
	}
	
	public void deleteCustomer(M_Customer_Relation c) {
		cr.save(c);
	}
	
	public List<M_Customer_Relation> searchCustomer(String name) {
		return cr.searchByName(name);
	}

}
