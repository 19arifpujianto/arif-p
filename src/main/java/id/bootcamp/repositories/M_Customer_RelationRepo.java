package id.bootcamp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.bootcamp.models.M_Customer_Relation;

public interface M_Customer_RelationRepo extends JpaRepository<M_Customer_Relation, Long>{

	@Query(value = "select * from m_customer_relation where is delete = false", nativeQuery = true)
	List<M_Customer_Relation> getAllCustomer();
	
	@Query(value = "SELECT * FROM m_customer_relation WHERE is_delete = false and id = :cari", nativeQuery = true)
	M_Customer_Relation getCustomerById(Long cari);
	
	@Query(value = "SELECT * FROM m_customer_relation WHERE is_delete = false AND name ILIKE CONCAT('%',:search,'%')", nativeQuery = true)
    public List<M_Customer_Relation> searchByName(String search);
	
}
