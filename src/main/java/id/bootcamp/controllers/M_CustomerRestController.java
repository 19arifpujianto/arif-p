package id.bootcamp.controllers;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.hibernate.criterion.Distinct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.sym.Name1;

import id.bootcamp.models.M_Customer;
import id.bootcamp.models.M_Customer_Relation;
import id.bootcamp.services.M_Customer_RelationServ;

@RestController
@RequestMapping("customerApi")
public class M_CustomerRestController {

	@Autowired
	private M_Customer_RelationServ cs;
	
	@GetMapping("getAllCustomer")
	public List<M_Customer_Relation> getAllCustomer(){
		List<M_Customer_Relation> cusre = cs.getAllCustomer();
		
		List<M_Customer_Relation> sorted = cusre.stream()
				.filter(cust -> cust.getIs_delete() == false)
//				.distinct()
//				.sorted(Comparator.comparingLong(M_Customer_Relation::getId))
//				.sorted((n1,n2) -> (int)(n1.getName()-n2.getName()))
				.sorted((id1,id2) -> (id2.getId().compareTo(id1.getId())))
//				.findAny()
//				.map(String::toUpperCase)
				.collect(Collectors.toList());
		
		return sorted;
	}
	
	@GetMapping("getCustomer/{id}")
	public M_Customer_Relation getCustomerById(@PathVariable("id")Long id) {
		return cs.getCustomerById(id);
	}
	
	@GetMapping("getCustomers/search")
	public List<M_Customer_Relation> searchCustomer(@RequestParam("name") String name) {
		return cs.searchCustomer(name);
	}
	
	@PostMapping("addCustomer")
	public String addCustomer(@RequestBody M_Customer_Relation cm) {
		cm.setCreated_by(1L);
		cm.setCreated_on(new Date());
		cs.addCustomer(cm);
		
		return "ok";
	}
	
	@PutMapping("editCustomer")
	public String editCustomer(@RequestBody M_Customer_Relation cm) {
		M_Customer_Relation custSebelumnya = cs.getCustomerById(cm.getId());
		cm.setCreated_by(custSebelumnya.getCreate_by());
		cm.setCreated_on(custSebelumnya.getCreate_on());
		
		cm.setModified_by(1L);
		cm.setModified_on(new Date());
		cs.editCustomer(cm);
		return "ok";
	}
	
	@PutMapping("deleteCustomer/{id}")
	public String deleteCustomer(@PathVariable("id") Long id) {
		M_Customer_Relation custSebelumnya = cs.getCustomerById(id);
		custSebelumnya.setIs_delete(true);
		
		custSebelumnya.setDeleted_by(id);
		custSebelumnya.setDeleted_on(new Date());
		cs.deleteCustomer(custSebelumnya);
		
		return "ok";
	}
}
