INSERT INTO m_user(created_by, created_on)
        VALUES (1, Now()),
                (2,now()),
                (3,now());

insert into m_customer (biodata_id,dob,gender,blood_group_id,rhesus_type,height,weight,created_by,created_on)
values (null,to_date('11-05-2000','dd-MM-yyyy'),'L',1,'A',165,65,1,now()),
        (null,to_date('22-02-1999','dd-MM-yyyy'),'P',2,'O',154,60,2,now()),
        (null,to_date('10-01-2000','dd-MM-yyyy'),'P',3,'AB',160,60,3,now());

insert into m_customer_relation (name,created_by,created_on)
VALUES ('Bapak',1, Now()),
        ('Ibu',2,now()),
        ('Ibu',3,now());

insert into m_customer_member (parent_biodata_id,customer_id,customer_relation_id,created_by,created_on)
values (null,1,1,1,now()),
        (null,2,2,2,now()),
        (null,3,3,3,now());